package cmd

import (
	"context"
	"flag"
	"os"
	"os/signal"
	"strings"
	"syscall"

	"github.com/mitchellh/go-homedir"
	"github.com/spf13/cobra"
	"github.com/spf13/pflag"
	"github.com/spf13/viper"
	"k8s.io/klog/v2"
)

// RootCmd sets up a standard root command template
// Handles signal termination with context and Run handler
type RootCmd struct {
	// Use text for command
	Use string
	// Short command description
	Short string
	// Long command description
	Long string
	// Run function takes process cancellation context and args
	// return an error if we should exit non-zero
	Run func(ctx context.Context, args []string) error
	// EnvPrefix prefix for auto environment variables
	EnvPrefix string
	// InitFlags called if set with flagset for command
	InitFlags func(*cobra.Command, *pflag.FlagSet)
	// ConfigFlag if config required
	ConfigFlag string
	// ConfigFileName for use when searching for config
	ConfigFileName string

	configFile string
}

// Execute RootCmd, parsing command line flags and calling Run
func (c RootCmd) Execute() {
	cmd := &cobra.Command{
		Use:   c.Use,
		Short: c.Short,
		Long:  c.Long,
		Run:   c.run,
	}
	if err := c.initFlags(cmd); err != nil {
		klog.Exit(err)
	}

	if err := cmd.Execute(); err != nil {
		klog.Exit(err)
	}
}

func (c RootCmd) run(cmd *cobra.Command, args []string) {
	defer klog.Flush()

	ctx, cf := context.WithCancel(context.Background())
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, os.Interrupt, syscall.SIGTERM)
	go func() {
		<-sigs
		cf()
	}()

	if err := c.Run(ctx, args); err != nil {
		klog.Fatal(err)
	}
}

func (c RootCmd) initFlags(cmd *cobra.Command) error {
	// coexist with packages using glog global flags
	_ = flag.Set("alsologtostderr", "true")

	klogFlags := flag.NewFlagSet("klog", flag.ExitOnError)
	klog.InitFlags(klogFlags)

	cobra.OnInitialize(c.initConfig)
	fs := cmd.Flags()
	fs.AddGoFlagSet(klogFlags)
	// Add support for go default flagset
	fs.AddGoFlagSet(flag.CommandLine)
	// HACK: Hides global flags e.g. those set by other libs
	fs.VisitAll(func(f *pflag.Flag) {
		if klogFlags.Lookup(f.Name) == nil {
			f.Hidden = true
		}
	})

	if c.ConfigFlag != "" {
		fs.StringVar(&c.configFile, c.ConfigFlag, "", "config file override.")
	}

	if c.InitFlags != nil {
		c.InitFlags(cmd, fs)
	}

	// Bind viper to list of flags
	if err := viper.BindPFlags(fs); err != nil {
		klog.Error(err)
	}

	return nil
}

// initConfig reads in config file and ENV variables if set.
func (c *RootCmd) initConfig() {
	if c.configFile != "" {
		// Use config file from the flag.
		viper.SetConfigFile(c.configFile)
	} else if c.ConfigFileName != "" {
		// Find home directory.
		home, err := homedir.Dir()
		if err != nil {
			klog.Exit(err)
		}

		viper.AddConfigPath(home)
		viper.AddConfigPath(".")
		// workspace configs folder
		viper.AddConfigPath("./configs")
		viper.SetConfigName(c.ConfigFileName)
	}

	// env key replacer allows nested environment variables, e.g. COLLECTOR_PERF_BASEPATH=something
	viper.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))
	if c.EnvPrefix != "" {
		viper.SetEnvPrefix(c.EnvPrefix)
	}
	viper.AutomaticEnv() // read in environment variables that match

	// If a config file is found, read it in.
	if err := viper.ReadInConfig(); err == nil {
		klog.V(1).Infof("Using config file: %s", viper.ConfigFileUsed())
	}
}
