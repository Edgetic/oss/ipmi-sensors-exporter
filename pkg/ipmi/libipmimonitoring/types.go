/*
	Type implementations for libipmimonitoring
	See http://git.savannah.gnu.org/cgit/freeipmi.git/tree/libipmimonitoring/ipmi_monitoring.h.in
*/

package libipmimonitoring

// Protocol versions
const (
	ProtocolVersion15 ProtocolVersion = iota
	ProtocolVersion20
)

// Sensor reading types
const (
	SensorReadingTypeBool SensorReadingType = iota
	SensorReadingTypeInt32
	SensorReadingTypeFloat64
	SensorReadingTypeUnknown SensorReadingType = 255
)

// Sensor types
const (
	SensorTypeReserved SensorType = iota
	SensorTypeTemperature
	SensorTypeVoltage
	SensorTypeCurrent
	SensorTypeFan
	SensorTypePhysicalSecurity
	SensorTypePlatformSecurityViolationAttempt
	SensorTypeProcessor
	SensorTypePowerSupply
	SensorTypePowerUnit
	SensorTypeCoolingDevice
	SensorTypeOtherUnitsBasedSensor
	SensorTypeMemory
	SensorTypeDriveSlot
	SensorTypePostMemoryResize
	SensorTypeSystemFirmwareProgress
	SensorTypeEventLoggingDisabled
	SensorTypeWatchdog1
	SensorTypeSystemEvent
	SensorTypeCriticalInterrupt
	SensorTypeButtonSwitch
	SensorTypeModuleBoard
	SensorTypeMicrocontrollerCoprocessor
	SensorTypeAddInCard
	SensorTypeChassis
	SensorTypeChipSet
	SensorTypeOtherFRU
	SensorTypeCableInterconnect
	SensorTypeTerminator
	SensorTypeSystemBootInitiated
	SensorTypeBootError
	SensorTypeOSBoot
	SensorTypeOSCriticalStop
	SensorTypeSlotConnector
	SensorTypeSystemACPIPowerState
	SensorTypeWatchdog2
	SensorTypePlatformAlert
	SensorTypeEntityPresence
	SensorTypeMonitorASICIC
	SensorTypeLan
	SensorTypeManagementSubsystemHealth
	SensorTypeBattery
	SensorTypeSessionAudit
	SensorTypeVersionChange
	SensorTypeFRUState
	// 0x2D - 0xbf - Reserved
	// 0xC0 - 0xff - Oem sensor types
	SensorTypeOemMin  SensorType = 192
	SensorTypeOemMax  SensorType = 255
	SensorTypeUnknown SensorType = 65535
)

// Sensor units for readings
const (
	SensorUnitsNone SensorUnits = iota
	SensorUnitsCelsius
	SensorUnitsFahrenheit
	SensorUnitsVolts
	SensorUnitsAmps
	SensorUnitsRPM
	SensorUnitsWatts
	SensorUnitsPercent
	SensorUnitsUnknown SensorUnits = 255
)

// Privilege level for remote access
const (
	PrivilegeLevelUser PrivilegeLevel = iota
	PrivilegeLevelOperator
	PrivilegeLevelAdmin
)

// Authentication types
const (
	AuthenticationTypeNone AuthenticationType = iota
	AuthenticationTypeStraightPasswordKey
	AuthenticationTypeMD2
	AuthenticationTypeMD5
)

// Workaround flags for protocol 1.5
const (
	// For use with Ipmi Monitoring Protocol Version 1.5
	WorkaroundFlagsAcceptSessionIdZero WorkaroundFlags = 1 << (iota + 1)
	WorkaroundFlagsForcePermsgAuthentication
	WorkaroundFlagsCheckUnexpectedAuthcode
	WorkaroundFlagsBigEndianSequenceNumber
	WorkaroundFlagsNoAuthCodeCheck
)

// Workaround flags for protocol 2.0
const (
	WorkaroundFlagsIntel20Session WorkaroundFlags = 1 << (iota + 1)
	WorkaroundFlagsSupermicro20Session
	WorkaroundFlagsSun20Session
	WorkaroundFlagsOpenSessionPrivilege
	WorkaroundFlagsNonEmptyIntegrityCheckValue
)

// Workaround flags for both protocols
const (
	WorkaroundFlagsNone                       WorkaroundFlags = 0
	WorkaroundFlagsAuthenticationCapabilities WorkaroundFlags = 1
	WorkaroundFlagsNoChecksumCheck            WorkaroundFlags = 64
)

// Sensor reading flags
const (
	SensorReadingFlagsRereadSDRCache SensorReadingFlags = 1 << iota
	SensorReadingFlagsIgnoreNonInterpretableSensors
	SensorReadingFlagsBridgeSensors
	SensorReadingFlagsInterpretOemData
	SensorReadingFlagsSharedSensors
	SensorReadingFlagsDiscreteReading
	SensorReadingFlagsIgnoreScanningDisabled
	SensorReadingFlagsAssumeBMCOwner
	SensorReadingFlagsEntitySensorNames
	SensorReadingFlagsAssumeMaxSDRRecordCount
)

// CipherSuite ids
// grouped into 3 values, Authentication Algorithm, Integrity Alg & Confidentiality Alg
const (
	// 0 - A = None; I = None; C = None
	CipherSuiteNone CipherSuite = iota
	// 1 - A = HMAC-SHA1; I = None; C = None
	CipherSuiteHMACSHA1
	// 2 - A = HMAC-SHA1; I = HMAC-SHA1-96; C = None
	CipherSuiteHMACSHA1_96
	// 3 - A = HMAC-SHA1; I = HMAC-SHA1-96; C = AES-CBC-128
	CipherSuiteHMACSHA1_96_AESCBC128
	// 6 - A = HMAC-MD5; I = None; C = None
	CipherSuiteHMACMD5 CipherSuite = iota + 2
	//  7 - A = HMAC-MD5; I = HMAC-MD5-128; C = None
	CipherSuiteHMACMD5_128
	//  8 - A = HMAC-MD5; I = HMAC-MD5-128; C = AES-CBC-128
	CipherSuiteHMACMD5_128_AESCBC128
	//  11 - A = HMAC-MD5; I = MD5-128; C = None
	CipherSuiteHMACMD5_MD5128 CipherSuite = iota + 4
	//  12 - A = HMAC-MD5; I = MD5-128; C = AES-CBC-128
	CipherSuiteHMACMD5_MD5128_AESCBC128
	//  15 - A = HMAC-SHA256; I = None; C = None
	CipherSuiteHMACSHA256 CipherSuite = iota + 6
	//  16 - A = HMAC-SHA256; I = HMAC-SHA256-128; C = None
	CipherSuiteHMACSHA256_128
	//  17 - A = HMAC-SHA256; I = HMAC-SHA256-128; C = AES-CBC-128
	CipherSuiteHMACSHA256_128_AESCBC128
)

// ProtocolVersion for IPMI connections
type ProtocolVersion int

// SensorReading type represents type of sensor reading
type SensorReadingType int

// SensorType representing type of sensor
type SensorType int

// SensorUnits representing unit type
type SensorUnits int

// PrivilegeLevel for requests
type PrivilegeLevel int

// AuthenticationType choice for auth
type AuthenticationType int

// WorkaroundFlags for known workarounds
type WorkaroundFlags int

// SensorReadingFlags settings for sensor reading
type SensorReadingFlags int

// CipherSuite for auth, integrity and confidentiality
type CipherSuite uint

// SensorReading represents sensor read from SDR
type SensorReading struct {
	// Name of sensor
	Name string
	// RecordId id from SDR
	RecordId int
	// Number of sensor, can have shared RecordId
	Number int
	// Type of sensor
	Type SensorType
	// Units reported by reading
	Units SensorUnits
	// ReadingType is numeric type of reading
	ReadingType SensorReadingType
	// Reading holds primitive reading value
	// one of bool, int32 or float64
	Reading interface{}
}
