package libipmimonitoring

// #cgo pkg-config: libipmimonitoring
// #include <stdlib.h>
// #include <ipmi_monitoring.h>
import "C"
import (
	"errors"
	"fmt"
	"path/filepath"
	"sync"
	"unsafe"
)

// ContextConfig used to set up a monitoring context
type ContextConfig struct {
	// ProtocolVersion if version of LAN IPMI to use
	ProtocolVersion ProtocolVersion
	// Host to connect to
	Host string
	// Username for auth
	Username string
	// Password for auth
	Password string
	// CacheDir caching directory for SDR info files
	// needs to be writable by this process
	CacheDir string
	// PrivilegeLevel used for context session
	PrivilegeLevel PrivilegeLevel
	// AuthenticationType for context auth
	AuthenticationType AuthenticationType
	// CipherSuite id of Cipher suite
	CipherSuite CipherSuite
	// WorkaroundFlags used for weird BMC implementations
	WorkaroundFlags WorkaroundFlags
}

// IPMIManager provides access to contexts and inits
type IPMIManager interface {
	// Init IPMIMonitor to be called once before getting contexts
	Init() error
	// SensorContext gets an IPMIContext to be used by threads
	SensorContext(ContextConfig) (IPMISensorContext, error)
}

// IPMISensorContext provides sensor monitoring functions
// Call Close when no longer required
type IPMISensorContext interface {
	// ReadSensors reads sensors from IPMI server
	ReadSensors() ([]SensorReading, error)
	// Close context and free resources
	Close()
}

// DefaultConfig provides defaults as set in libipmimonitoring
// except that we set protocol V2.0
func DefaultConfig() ContextConfig {
	return ContextConfig{
		ProtocolVersion:    ProtocolVersion20,
		PrivilegeLevel:     PrivilegeLevelUser,
		AuthenticationType: AuthenticationTypeMD5,
		CipherSuite:        CipherSuiteHMACSHA1_96_AESCBC128,
	}
}

func NewManager() IPMIManager {
	return &manager{}
}

type manager struct {
	init     sync.Once
	initDone bool
	initErr  error
}

func (m *manager) Init() error {
	m.init.Do(func() {
		var errno C.int
		code := C.ipmi_monitoring_init(C.IPMI_MONITORING_FLAGS_NONE, &errno)
		if code == -1 {
			m.initErr = fmt.Errorf("Error initialising IPMI monitoring: %w", ipmiStrError(code))
		}
		m.initDone = true
	})

	return m.initErr
}

func (m *manager) SensorContext(config ContextConfig) (IPMISensorContext, error) {
	if m.initErr != nil {
		return nil, m.initErr
	}
	if !m.initDone {
		return nil, errors.New("Init function has not been called")
	}

	ctx, err := C.ipmi_monitoring_ctx_create()
	if ctx == nil {
		return nil, fmt.Errorf("Error creating ipmi context: %v", err)
	}
	context := &context{
		ctx:      ctx,
		config:   makeIPMIConfig(config),
		hostname: config.Host,
	}

	cd, _ := filepath.Abs(config.CacheDir)
	cacheDir := C.CString(cd)
	defer C.free(unsafe.Pointer(cacheDir))

	if config.CacheDir != "" {
		code := C.ipmi_monitoring_ctx_sdr_cache_directory(ctx, cacheDir)
		if code == -1 {
			defer context.Close()
			return nil, fmt.Errorf("Error setting up context cache dir %s: %w", config.CacheDir, context.ctxError())
		}
	}

	return context, nil
}

func ipmiStrError(errno C.int) error {
	cs := C.ipmi_monitoring_ctx_strerror(errno)
	msg := C.GoString(cs)
	return errors.New(msg)
}

func makeIPMIConfig(config ContextConfig) C.struct_ipmi_monitoring_ipmi_config {
	// just remote settings applied here
	return C.struct_ipmi_monitoring_ipmi_config{
		protocol_version:    C.int(config.ProtocolVersion),
		username:            C.CString(config.Username),
		password:            C.CString(config.Password),
		privilege_level:     C.int(config.PrivilegeLevel),
		authentication_type: C.int(config.AuthenticationType),
		cipher_suite_id:     C.int(config.CipherSuite),
		workaround_flags:    C.uint(config.WorkaroundFlags),
	}
}

func cleanupIPMIConfig(config C.struct_ipmi_monitoring_ipmi_config) {
	if config.username != nil {
		C.free(unsafe.Pointer(config.username))
	}
	if config.password != nil {
		C.free(unsafe.Pointer(config.password))
	}
}

type context struct {
	config   C.struct_ipmi_monitoring_ipmi_config
	ctx      C.ipmi_monitoring_ctx_t
	hostname string
}

func (c *context) ctxError() error {
	cs := C.ipmi_monitoring_ctx_errormsg(c.ctx)
	msg := C.GoString(cs)
	return errors.New(msg)
}

func (c *context) Close() {
	cleanupIPMIConfig(c.config)
	C.ipmi_monitoring_ctx_destroy(c.ctx)
}

func (c *context) ReadSensors() ([]SensorReading, error) {
	hostStr := C.CString(c.hostname)
	defer C.free(unsafe.Pointer(hostStr))
	code := C.ipmi_monitoring_sensor_readings_by_record_id(c.ctx, hostStr, &c.config,
		// sensor reading flags might need amending later
		C.IPMI_MONITORING_SENSOR_READING_FLAGS_IGNORE_NON_INTERPRETABLE_SENSORS|
			C.IPMI_MONITORING_SENSOR_READING_FLAGS_BRIDGE_SENSORS|
			C.IPMI_MONITORING_SENSOR_READING_FLAGS_SHARED_SENSORS,
		nil, 0, nil, nil)
	if code == -1 {
		return nil, fmt.Errorf("Error getting ipmi sensor readings: %w", c.ctxError())
	}
	defer C.ipmi_monitoring_sensor_iterator_destroy(c.ctx)

	return c.readSensors()
}

func (c *context) readSensors() ([]SensorReading, error) {
	ss := []SensorReading{}
	for {
		sens, err := c.decodeCurrentSensorReading()
		if err != nil {
			return nil, fmt.Errorf("Error decoding sensor reading: %w", err)
		}
		ss = append(ss, *sens)
		next := C.ipmi_monitoring_sensor_iterator_next(c.ctx)
		if next == 0 {
			break
		}
		if next == -1 {
			return nil, fmt.Errorf("Error iterating sensor context readings: %w", c.ctxError())
		}
	}
	return ss, nil
}

func (c *context) decodeCurrentSensorReading() (*SensorReading, error) {
	recordId := C.ipmi_monitoring_sensor_read_record_id(c.ctx)
	if recordId == -1 {
		return nil, fmt.Errorf("Error calling ipmi_monitoring_sensor_read_record_id: %w", c.ctxError())
	}
	nameChars := C.ipmi_monitoring_sensor_read_sensor_name(c.ctx)
	if nameChars == nil {
		return nil, fmt.Errorf("Error calling ipmi_monitoring_sensor_read_record_id: %w", c.ctxError())
	}
	name := C.GoString(nameChars)
	sensorReadingType := C.ipmi_monitoring_sensor_read_sensor_reading_type(c.ctx)
	if sensorReadingType == -1 {
		return nil, fmt.Errorf("Error calling ipmi_monitoring_sensor_read_sensor_reading_type: %w", c.ctxError())
	}
	sensorNumber := C.ipmi_monitoring_sensor_read_sensor_number(c.ctx)
	if sensorNumber == -1 {
		return nil, fmt.Errorf("Error calling ipmi_monitoring_sensor_read_sensor_number: %w", c.ctxError())
	}
	sensorType := C.ipmi_monitoring_sensor_read_sensor_type(c.ctx)
	if sensorType == -1 {
		return nil, fmt.Errorf("Error calling ipmi_monitoring_sensor_read_sensor_type: %w", c.ctxError())
	}
	sensorState := C.ipmi_monitoring_sensor_read_sensor_state(c.ctx)
	if sensorState == -1 {
		return nil, fmt.Errorf("Error calling ipmi_monitoring_sensor_read_sensor_state: %w", c.ctxError())
	}
	sensorUnits := C.ipmi_monitoring_sensor_read_sensor_units(c.ctx)
	if sensorUnits == -1 {
		return nil, fmt.Errorf("Error calling ipmi_monitoring_sensor_read_sensor_units: %w", c.ctxError())
	}
	// reading can be nil for events or unknown sesnors
	reading := C.ipmi_monitoring_sensor_read_sensor_reading(c.ctx)

	return &SensorReading{
		Name:        name,
		RecordId:    int(recordId),
		Number:      int(sensorNumber),
		Type:        SensorType(sensorType),
		Units:       SensorUnits(sensorUnits),
		ReadingType: SensorReadingType(sensorReadingType),
		Reading:     sensorReading(SensorReadingType(sensorReadingType), reading),
	}, nil
}

func sensorReading(t SensorReadingType, p unsafe.Pointer) interface{} {
	switch t {
	case SensorReadingTypeBool:
		i := (*uint8)(p)
		if *i == 1 {
			return true
		} else {
			return false
		}
	case SensorReadingTypeInt32:
		return *(*int32)(p)
	case SensorReadingTypeFloat64:
		return *(*float64)(p)
	}
	return nil
}
