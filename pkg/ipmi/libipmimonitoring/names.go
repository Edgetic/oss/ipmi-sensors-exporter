package libipmimonitoring

// FlagNamer interface provides name conversions for flag types
type FlagNamer interface {
	// Names slice of all flag names
	Names() []string
	// NameFlag returns int flag for name
	// -1 returned if not found
	NameFlag(string) int
	// FlagName returns name for flag int or false if not found
	FlagName(int) (string, bool)
}

var (
	protocolVersionNames = flagNames{
		int(ProtocolVersion15): "1.5",
		int(ProtocolVersion20): "2.0",
	}

	sensorReadingTypeNames = flagNames{
		int(SensorReadingTypeBool):    "bool",
		int(SensorReadingTypeInt32):   "int32",
		int(SensorReadingTypeFloat64): "float64",
		int(SensorReadingTypeUnknown): "unknown",
	}

	sensorTypeNames = flagNames{
		int(SensorTypeReserved):                         "reserved",
		int(SensorTypeTemperature):                      "temperature",
		int(SensorTypeVoltage):                          "voltage",
		int(SensorTypeCurrent):                          "current",
		int(SensorTypeFan):                              "fan",
		int(SensorTypePhysicalSecurity):                 "physical security",
		int(SensorTypePlatformSecurityViolationAttempt): "platform security violation attempt",
		int(SensorTypeProcessor):                        "processor",
		int(SensorTypePowerSupply):                      "power supply",
		int(SensorTypePowerUnit):                        "power unit",
		int(SensorTypeCoolingDevice):                    "cooling device",
		int(SensorTypeOtherUnitsBasedSensor):            "other units based sensor",
		int(SensorTypeMemory):                           "memory",
		int(SensorTypeDriveSlot):                        "drive slot",
		int(SensorTypePostMemoryResize):                 "post memory resize",
		int(SensorTypeSystemFirmwareProgress):           "system firmware progress",
		int(SensorTypeEventLoggingDisabled):             "event logging disabled",
		int(SensorTypeWatchdog1):                        "watchdog 1",
		int(SensorTypeSystemEvent):                      "system event",
		int(SensorTypeCriticalInterrupt):                "critical interrupt",
		int(SensorTypeButtonSwitch):                     "button switch",
		int(SensorTypeModuleBoard):                      "module board",
		int(SensorTypeMicrocontrollerCoprocessor):       "microcontroller coprocessor",
		int(SensorTypeAddInCard):                        "add in card",
		int(SensorTypeChassis):                          "chassis",
		int(SensorTypeChipSet):                          "chip set",
		int(SensorTypeOtherFRU):                         "other fru",
		int(SensorTypeCableInterconnect):                "cable interconnect",
		int(SensorTypeTerminator):                       "terminator",
		int(SensorTypeSystemBootInitiated):              "system boot initiated",
		int(SensorTypeBootError):                        "boot error",
		int(SensorTypeOSBoot):                           "os boot",
		int(SensorTypeOSCriticalStop):                   "os critical stop",
		int(SensorTypeSlotConnector):                    "slot connector",
		int(SensorTypeSystemACPIPowerState):             "system acpi power state",
		int(SensorTypeWatchdog2):                        "watchdog 2",
		int(SensorTypePlatformAlert):                    "platform alert",
		int(SensorTypeEntityPresence):                   "entity presence",
		int(SensorTypeMonitorASICIC):                    "monitor asic ic",
		int(SensorTypeLan):                              "lan",
		int(SensorTypeManagementSubsystemHealth):        "management subsystem health",
		int(SensorTypeBattery):                          "battery",
		int(SensorTypeSessionAudit):                     "session audit",
		int(SensorTypeVersionChange):                    "version change",
		int(SensorTypeFRUState):                         "fru state",
		int(SensorTypeOemMin):                           "oem min",
		int(SensorTypeOemMax):                           "oem max",
		int(SensorTypeUnknown):                          "unknown",
	}

	sensorUnitsNames = flagNames{
		int(SensorUnitsNone):       "none",
		int(SensorUnitsCelsius):    "celsius",
		int(SensorUnitsFahrenheit): "fahrenheit",
		int(SensorUnitsVolts):      "volts",
		int(SensorUnitsAmps):       "amps",
		int(SensorUnitsRPM):        "rpm",
		int(SensorUnitsWatts):      "watts",
		int(SensorUnitsPercent):    "percent",
		int(SensorUnitsUnknown):    "unknown",
	}

	privilegeLevelNames = flagNames{
		int(PrivilegeLevelUser):     "user",
		int(PrivilegeLevelOperator): "operator",
		int(PrivilegeLevelAdmin):    "admin",
	}

	authenticationTypeNames = flagNames{
		int(AuthenticationTypeNone):                "none",
		int(AuthenticationTypeStraightPasswordKey): "straight password key",
		int(AuthenticationTypeMD2):                 "md2",
		int(AuthenticationTypeMD5):                 "md5",
	}

	workaroundFlagNames = flagNames{
		int(WorkaroundFlagsNone):                       "none",
		int(WorkaroundFlagsAuthenticationCapabilities): "authentication capabilities",
		int(WorkaroundFlagsNoChecksumCheck):            "no checksum check",
	}

	workaroundFlagNames15 = flagNames{
		int(WorkaroundFlagsAcceptSessionIdZero):       "accept session id zero",
		int(WorkaroundFlagsForcePermsgAuthentication): "force permsg authentication",
		int(WorkaroundFlagsCheckUnexpectedAuthcode):   "check unexpected authcode",
		int(WorkaroundFlagsBigEndianSequenceNumber):   "big endian sequence number",
		int(WorkaroundFlagsNoAuthCodeCheck):           "no auth code check",
	}

	workaroundFlagNames20 = flagNames{
		int(WorkaroundFlagsIntel20Session):              "intel 20 session",
		int(WorkaroundFlagsSupermicro20Session):         "supermicro 20 session",
		int(WorkaroundFlagsSun20Session):                "sun 20 session",
		int(WorkaroundFlagsOpenSessionPrivilege):        "open session privilege",
		int(WorkaroundFlagsNonEmptyIntegrityCheckValue): "non empty integrity check value",
	}

	sensorReadingFlagNames = flagNames{
		int(SensorReadingFlagsRereadSDRCache):                "reread sdr cache",
		int(SensorReadingFlagsIgnoreNonInterpretableSensors): "ignore non interpretable sensors",
		int(SensorReadingFlagsBridgeSensors):                 "bridge sensors",
		int(SensorReadingFlagsInterpretOemData):              "interpret oem data",
		int(SensorReadingFlagsSharedSensors):                 "shared sensors",
		int(SensorReadingFlagsDiscreteReading):               "discrete reading",
		int(SensorReadingFlagsIgnoreScanningDisabled):        "ignore scanning disabled",
		int(SensorReadingFlagsAssumeBMCOwner):                "assume bmc owner",
		int(SensorReadingFlagsEntitySensorNames):             "entity sensor names",
		int(SensorReadingFlagsAssumeMaxSDRRecordCount):       "assume max sdr record count",
	}
)

func (p ProtocolVersion) Names() []string {
	return protocolVersionNames.names()
}

func (p ProtocolVersion) NameFlag(n string) int {
	return protocolVersionNames.nameToFlag(n)
}

func (p ProtocolVersion) FlagName(i int) (string, bool) {
	return protocolVersionNames.name(i)
}

func (s SensorReadingType) Names() []string {
	return sensorReadingTypeNames.names()
}

func (s SensorReadingType) NameFlag(n string) int {
	return sensorReadingTypeNames.nameToFlag(n)
}

func (s SensorReadingType) FlagName(i int) (string, bool) {
	return sensorReadingTypeNames.name(i)
}

func (s SensorReadingType) String() string {
	return flagName(s, int(s))
}

func (s SensorType) Names() []string {
	return sensorTypeNames.names()
}

func (s SensorType) NameFlag(n string) int {
	return sensorTypeNames.nameToFlag(n)
}

func (s SensorType) FlagName(i int) (string, bool) {
	return sensorTypeNames.name(i)
}

func (s SensorType) String() string {
	return flagName(s, int(s))
}

func (s SensorUnits) Names() []string {
	return sensorUnitsNames.names()
}

func (s SensorUnits) NameFlag(n string) int {
	return sensorUnitsNames.nameToFlag(n)
}

func (s SensorUnits) FlagName(i int) (string, bool) {
	return sensorUnitsNames.name(i)
}

func (s SensorUnits) String() string {
	return flagName(s, int(s))
}

func (s PrivilegeLevel) Names() []string {
	return privilegeLevelNames.names()
}

func (s PrivilegeLevel) NameFlag(n string) int {
	return privilegeLevelNames.nameToFlag(n)
}

func (s PrivilegeLevel) FlagName(i int) (string, bool) {
	return privilegeLevelNames.name(i)
}

func (a AuthenticationType) Names() []string {
	return authenticationTypeNames.names()
}

func (a AuthenticationType) NameFlag(n string) int {
	return authenticationTypeNames.nameToFlag(n)
}

func (a AuthenticationType) FlagName(i int) (string, bool) {
	return authenticationTypeNames.name(i)
}

// WorkaroundFlagNamer names WorkaroundFlags based on protocol version
type WorkaroundFlagNamer struct {
	// ProtocolVersion in use
	ProtocolVersion ProtocolVersion
}

func (w WorkaroundFlagNamer) Names() []string {
	switch w.ProtocolVersion {
	case ProtocolVersion15:
		return append(workaroundFlagNames.names(), workaroundFlagNames15.names()...)
	case ProtocolVersion20:
		return append(workaroundFlagNames.names(), workaroundFlagNames20.names()...)
	}
	return nil
}

func (w WorkaroundFlagNamer) NameFlag(n string) int {
	if f := workaroundFlagNames.nameToFlag(n); f != -1 {
		return f
	}

	switch w.ProtocolVersion {
	case ProtocolVersion15:
		return workaroundFlagNames15.nameToFlag(n)
	case ProtocolVersion20:
		return workaroundFlagNames20.nameToFlag(n)
	}
	return -1
}

func (w WorkaroundFlagNamer) FlagName(i int) (string, bool) {
	if n, ok := workaroundFlagNames.name(i); ok {
		return n, ok
	}

	switch w.ProtocolVersion {
	case ProtocolVersion15:
		return workaroundFlagNames15.name(i)
	case ProtocolVersion20:
		return workaroundFlagNames20.name(i)
	}
	return "", false
}

func (s SensorReadingFlags) Names() []string {
	return sensorReadingFlagNames.names()
}

func (s SensorReadingFlags) NameFlag(n string) int {
	return sensorReadingFlagNames.nameToFlag(n)
}

func (s SensorReadingFlags) FlagName(i int) (string, bool) {
	return sensorReadingFlagNames.name(i)
}

type flagNames map[int]string

func (f flagNames) names() []string {
	ns := make([]string, len(f))
	i := 0
	for _, v := range f {
		ns[i] = v
		i++
	}
	return ns
}

func (f flagNames) nameToFlag(n string) int {
	for k, v := range f {
		if v == n {
			return k
		}
	}
	return -1
}

func (f flagNames) name(flag int) (string, bool) {
	s, ok := f[flag]
	return s, ok
}

func flagName(fn FlagNamer, f int) string {
	s, ok := fn.FlagName(f)
	if !ok {
		return "unknown"
	}
	return s
}
