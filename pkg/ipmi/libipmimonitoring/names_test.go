package libipmimonitoring

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestProtocolVersion_Names(t *testing.T) {
	tests := []struct {
		name string
		p    ProtocolVersion
		want []string
	}{
		{
			"returns all",
			ProtocolVersion15,
			[]string{"1.5", "2.0"},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := tt.p.Names()
			assert.ElementsMatch(t, got, tt.want)
		})
	}
}

func TestProtocolVersion_NameFlag(t *testing.T) {
	type args struct {
		n string
	}
	tests := []struct {
		name string
		p    ProtocolVersion
		args args
		want int
	}{
		{
			"ProtocolVersion15",
			ProtocolVersion15,
			args{
				n: "1.5",
			},
			0,
		},
		{
			"ProtocolVersion20",
			ProtocolVersion20,
			args{
				n: "2.0",
			},
			1,
		},
		{
			"unknown foo is -1",
			ProtocolVersion15,
			args{
				n: "foo",
			},
			-1,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.p.NameFlag(tt.args.n); got != tt.want {
				t.Errorf("ProtocolVersion.NameFlag() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestProtocolVersion_FlagName(t *testing.T) {
	type args struct {
		i int
	}
	tests := []struct {
		name  string
		p     ProtocolVersion
		args  args
		want  string
		want1 bool
	}{
		{
			"1 is 2.0",
			ProtocolVersion15,
			args{
				int(ProtocolVersion20),
			},
			"2.0",
			true,
		},
		{
			"3 does not exist",
			ProtocolVersion15,
			args{
				3,
			},
			"",
			false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, got1 := tt.p.FlagName(tt.args.i)
			if got != tt.want {
				t.Errorf("ProtocolVersion.FlagName() got = %v, want %v", got, tt.want)
			}
			if got1 != tt.want1 {
				t.Errorf("ProtocolVersion.FlagName() got1 = %v, want %v", got1, tt.want1)
			}
		})
	}
}

func TestSensorReadingType_String(t *testing.T) {
	tests := []struct {
		name string
		s    SensorReadingType
		want string
	}{
		{
			"bool",
			SensorReadingTypeBool,
			"bool",
		},
		{
			"int32",
			SensorReadingTypeInt32,
			"int32",
		},
		{
			"float64",
			SensorReadingTypeFloat64,
			"float64",
		},
		{
			"unknown",
			SensorReadingTypeUnknown,
			"unknown",
		},
		{
			"actual unknown",
			SensorReadingType(44),
			"unknown",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.s.String(); got != tt.want {
				t.Errorf("SensorReadingType.String() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestWorkaroundFlagNamer_Names(t *testing.T) {
	type fields struct {
		ProtocolVersion ProtocolVersion
	}
	tests := []struct {
		name   string
		fields fields
		want   []string
	}{
		{
			"Protocol 1.5 workaround names",
			fields{
				ProtocolVersion15,
			},
			[]string{
				"none",
				"authentication capabilities",
				"no checksum check",
				"accept session id zero",
				"force permsg authentication",
				"check unexpected authcode",
				"big endian sequence number",
				"no auth code check",
			},
		},
		{
			"Protocol 2.0 workaround names",
			fields{
				ProtocolVersion20,
			},
			[]string{
				"none",
				"authentication capabilities",
				"no checksum check",
				"intel 20 session",
				"supermicro 20 session",
				"sun 20 session",
				"open session privilege",
				"non empty integrity check value",
			},
		},
		{
			"Invalid id returns nil",
			fields{
				ProtocolVersion(20),
			},
			nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			w := WorkaroundFlagNamer{
				ProtocolVersion: tt.fields.ProtocolVersion,
			}
			assert.ElementsMatch(t, tt.want, w.Names())
		})
	}
}

func TestWorkaroundFlagNamer_NameFlag(t *testing.T) {
	type fields struct {
		ProtocolVersion ProtocolVersion
	}
	type args struct {
		n string
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   int
	}{
		{
			"Shared name tag returns for 1.5",
			fields{
				ProtocolVersion15,
			},
			args{
				"no checksum check",
			},
			int(WorkaroundFlagsNoChecksumCheck),
		},
		{
			"Shared name tag returns for 2.0",
			fields{
				ProtocolVersion15,
			},
			args{
				"no checksum check",
			},
			int(WorkaroundFlagsNoChecksumCheck),
		},
		{
			"Name tag specific to 1.5 returns",
			fields{
				ProtocolVersion15,
			},
			args{
				"force permsg authentication",
			},
			int(WorkaroundFlagsForcePermsgAuthentication),
		},
		{
			"Name tag specific to 2.0 returns",
			fields{
				ProtocolVersion20,
			},
			args{
				"open session privilege",
			},
			int(WorkaroundFlagsOpenSessionPrivilege),
		},
		{
			"Invalid name tag returns -1",
			fields{
				ProtocolVersion15,
			},
			args{
				"potato",
			},
			-1,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			w := WorkaroundFlagNamer{
				ProtocolVersion: tt.fields.ProtocolVersion,
			}
			if got := w.NameFlag(tt.args.n); got != tt.want {
				t.Errorf("WorkaroundFlagNamer.NameFlag() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestWorkaroundFlagNamer_FlagName(t *testing.T) {
	type fields struct {
		ProtocolVersion ProtocolVersion
	}
	type args struct {
		i int
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   string
		want1  bool
	}{
		{
			"Shared tag name for 1.5",
			fields{
				ProtocolVersion15,
			},
			args{
				int(WorkaroundFlagsAuthenticationCapabilities),
			},
			"authentication capabilities",
			true,
		},
		{
			"Shared tag name for 2.0",
			fields{
				ProtocolVersion20,
			},
			args{
				int(WorkaroundFlagsAuthenticationCapabilities),
			},
			"authentication capabilities",
			true,
		},
		{
			"Tag name for 1.5 returned",
			fields{
				ProtocolVersion15,
			},
			args{
				int(WorkaroundFlagsAcceptSessionIdZero),
			},
			"accept session id zero",
			true,
		},
		{
			"Tag name for 2.0 returned",
			fields{
				ProtocolVersion20,
			},
			args{
				int(WorkaroundFlagsIntel20Session),
			},
			"intel 20 session",
			true,
		},
		{
			"Invalid tag name returns false",
			fields{
				ProtocolVersion20,
			},
			args{
				int(3),
			},
			"",
			false,
		},
		{
			"Protocol version invalid returns false",
			fields{
				ProtocolVersion(-1),
			},
			args{
				int(WorkaroundFlagsSupermicro20Session),
			},
			"",
			false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			w := WorkaroundFlagNamer{
				ProtocolVersion: tt.fields.ProtocolVersion,
			}
			got, got1 := w.FlagName(tt.args.i)
			if got != tt.want {
				t.Errorf("WorkaroundFlagNamer.FlagName() got = %v, want %v", got, tt.want)
			}
			if got1 != tt.want1 {
				t.Errorf("WorkaroundFlagNamer.FlagName() got1 = %v, want %v", got1, tt.want1)
			}
		})
	}
}
