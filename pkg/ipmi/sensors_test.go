// TODO: this package will become a prometheus exporter

package ipmi

import (
	"errors"
	"reflect"
	"testing"

	lib "gitlab.com/edgetic/oss/ipmi-sensors-exporter/pkg/ipmi/libipmimonitoring"
)

type ipmiManagerMock struct {
	lib.IPMIManager
	err bool
	c   lib.ContextConfig
}

func (m ipmiManagerMock) Init() error {
	if m.err {
		return errors.New("Init err")
	}
	return nil
}

func (m *ipmiManagerMock) SensorContext(c lib.ContextConfig) (lib.IPMISensorContext, error) {
	if m.err {
		return nil, errors.New("SensorContext err")
	}
	m.c = c
	return &ipmiContextMock{}, nil
}

func TestManager_Init(t *testing.T) {
	type fields struct {
		manager lib.IPMIManager
	}
	tests := []struct {
		name    string
		fields  fields
		wantErr bool
	}{
		{
			"ipmi init error returned",
			fields{
				&ipmiManagerMock{
					err: true,
				},
			},
			true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := Manager{
				manager: tt.fields.manager,
			}
			if err := m.Init(); (err != nil) != tt.wantErr {
				t.Errorf("Manager.Init() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestManager_Context(t *testing.T) {
	type fields struct {
		manager *ipmiManagerMock
	}
	type args struct {
		c SensorsConfig
	}
	tests := []struct {
		name       string
		fields     fields
		args       args
		want       lib.IPMISensorContext
		wantErr    bool
		wantConfig lib.ContextConfig
	}{
		{
			"config with default empty config returns error",
			fields{
				&ipmiManagerMock{},
			},
			args{},
			nil,
			true,
			lib.ContextConfig{},
		},
		{
			"default config with host field set returns",
			fields{
				&ipmiManagerMock{},
			},
			args{
				SensorsConfig{
					Host: "foo",
				},
			},
			&ipmiContextMock{},
			false,
			func() lib.ContextConfig {
				c := lib.DefaultConfig()
				c.Host = "foo"
				return c
			}(),
		},
		{
			"protocol version must be valid",
			fields{
				&ipmiManagerMock{},
			},
			args{
				SensorsConfig{
					Host:            "foo",
					ProtocolVersion: "potato",
				},
			},
			nil,
			true,
			lib.ContextConfig{},
		},
		{
			"privilege level must be valid",
			fields{
				&ipmiManagerMock{},
			},
			args{
				SensorsConfig{
					Host:           "foo",
					PrivilegeLevel: "potato",
				},
			},
			nil,
			true,
			lib.ContextConfig{},
		},
		{
			"authentication type must be valid",
			fields{
				&ipmiManagerMock{},
			},
			args{
				SensorsConfig{
					Host:               "foo",
					AuthenticationType: "potato",
				},
			},
			nil,
			true,
			lib.ContextConfig{},
		},
		{
			"cipher suite must be valid",
			fields{
				&ipmiManagerMock{},
			},
			args{
				SensorsConfig{
					Host: "foo",
					CipherSuite: func() *int {
						i := 4
						return &i
					}(),
				},
			},
			nil,
			true,
			lib.ContextConfig{},
		},
		{
			"workaround flags must be valid",
			fields{
				&ipmiManagerMock{},
			},
			args{
				SensorsConfig{
					Host:            "foo",
					WorkaroundFlags: []string{"none", "orange"},
				},
			},
			nil,
			true,
			lib.ContextConfig{},
		},
		{
			"all non-default field values can be set successfully",
			fields{
				&ipmiManagerMock{},
			},
			args{
				SensorsConfig{
					Host:               "foo",
					ProtocolVersion:    "1.5",
					WorkaroundFlags:    []string{"authentication capabilities", "accept session id zero"},
					Username:           "user",
					Password:           "pass",
					CacheDir:           "./testdata",
					PrivilegeLevel:     "Admin",
					AuthenticationType: "straight password key",
					CipherSuite: func() *int {
						i := 6
						return &i
					}(),
				},
			},
			&ipmiContextMock{},
			false,
			lib.ContextConfig{
				ProtocolVersion:    lib.ProtocolVersion15,
				Host:               "foo",
				Username:           "user",
				Password:           "pass",
				CacheDir:           "./testdata",
				PrivilegeLevel:     lib.PrivilegeLevelAdmin,
				AuthenticationType: lib.AuthenticationTypeStraightPasswordKey,
				CipherSuite:        lib.CipherSuiteHMACMD5,
				WorkaroundFlags:    lib.WorkaroundFlagsAuthenticationCapabilities | lib.WorkaroundFlagsAcceptSessionIdZero,
			},
		},
		{
			"sensor context creation error is returned",
			fields{
				&ipmiManagerMock{
					err: true,
				},
			},
			args{
				SensorsConfig{
					Host: "foo",
				},
			},
			nil,
			true,
			lib.ContextConfig{},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := Manager{
				manager: tt.fields.manager,
			}
			got, err := m.Context(tt.args.c)
			if (err != nil) != tt.wantErr {
				t.Errorf("Manager.Context() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Manager.Context() = %v, want %v", got, tt.want)
			}
			if !reflect.DeepEqual(tt.fields.manager.c, tt.wantConfig) {
				t.Errorf("Want config %v got %v", tt.fields.manager.c, tt.wantConfig)
			}
		})
	}
}
