package ipmi

import (
	"errors"
	"testing"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/stretchr/testify/assert"
	lib "gitlab.com/edgetic/oss/ipmi-sensors-exporter/pkg/ipmi/libipmimonitoring"
)

type ipmiContextMock struct {
	lib.IPMISensorContext
	err  bool
	sens []lib.SensorReading
}

func (m *ipmiContextMock) ReadSensors() ([]lib.SensorReading, error) {
	if m.err {
		return nil, errors.New("ReadSensors err")
	}
	return m.sens, nil
}

func TestPrometheusCollector_Collect(t *testing.T) {
	type fields struct {
		Context     lib.IPMISensorContext
		MachineName string
		Hostname    string
	}
	type args struct {
		metrics chan prometheus.Metric
	}
	tests := []struct {
		name        string
		fields      fields
		args        args
		wantMetrics []prometheus.Metric
	}{
		{
			"context reading errors handled",
			fields{
				&ipmiContextMock{
					err: true,
				},
				"foo",
				"bar",
			},
			args{
				make(chan prometheus.Metric, 10),
			},
			[]prometheus.Metric{
				prometheus.NewInvalidMetric(
					prometheus.NewDesc(prometheus.BuildFQName("ipmi_sensors", "", "error"), "", nil, nil),
					errors.New("ReadSensors err")),
			},
		},
		{
			"handles nil sensors",
			fields{
				Context: &ipmiContextMock{},
			},
			args{
				make(chan prometheus.Metric, 10),
			},
			[]prometheus.Metric{},
		},
		{
			"handles sensors of all types and ignores unknown",
			fields{
				&ipmiContextMock{
					sens: []lib.SensorReading{
						{
							Name:        "n1",
							RecordId:    1,
							Number:      1,
							Type:        lib.SensorTypeBattery,
							Units:       lib.SensorUnitsUnknown,
							ReadingType: lib.SensorReadingTypeUnknown,
							Reading:     nil,
						},
						{
							Name:        "n2",
							RecordId:    2,
							Number:      2,
							Type:        lib.SensorTypeChipSet,
							Units:       lib.SensorUnitsPercent,
							ReadingType: lib.SensorReadingTypeBool,
							Reading:     false,
						},
						{
							Name:        "n2",
							RecordId:    2,
							Number:      3,
							Type:        lib.SensorTypeChipSet,
							Units:       lib.SensorUnitsPercent,
							ReadingType: lib.SensorReadingTypeBool,
							Reading:     true,
						},
						{
							Name:        "n3",
							RecordId:    3,
							Number:      1,
							Type:        lib.SensorTypeTemperature,
							Units:       lib.SensorUnitsCelsius,
							ReadingType: lib.SensorReadingTypeFloat64,
							Reading:     float64(37.4),
						},
						{
							Name:        "n4",
							RecordId:    3,
							Number:      2,
							Type:        lib.SensorTypeFRUState,
							Units:       lib.SensorUnitsUnknown,
							ReadingType: lib.SensorReadingTypeInt32,
							Reading:     int32(2),
						},
					},
				},
				"bar",
				"foo",
			},
			args{
				make(chan prometheus.Metric, 10),
			},
			[]prometheus.Metric{
				prometheus.MustNewConstMetric(
					prometheus.NewDesc("ipmi_sensors_chip_set", "IPMI sensor reading for type chip set",
						[]string{"record_id", "number", "name", "units"},
						prometheus.Labels{
							"hostname": "foo",
							"machine":  "bar",
						}),
					prometheus.GaugeValue,
					0, []string{"2", "2", "n2", "percent"}...,
				),
				prometheus.MustNewConstMetric(
					prometheus.NewDesc("ipmi_sensors_chip_set", "IPMI sensor reading for type chip set",
						[]string{"record_id", "number", "name", "units"},
						prometheus.Labels{
							"hostname": "foo",
							"machine":  "bar",
						}),
					prometheus.GaugeValue,
					1, []string{"2", "3", "n2", "percent"}...,
				),
				prometheus.MustNewConstMetric(
					prometheus.NewDesc("ipmi_sensors_temperature", "IPMI sensor reading for type temperature",
						[]string{"record_id", "number", "name", "units"},
						prometheus.Labels{
							"hostname": "foo",
							"machine":  "bar",
						}),
					prometheus.GaugeValue,
					37.4, []string{"3", "1", "n3", "celsius"}...,
				),
				prometheus.MustNewConstMetric(
					prometheus.NewDesc("ipmi_sensors_fru_state", "IPMI sensor reading for type fru state",
						[]string{"record_id", "number", "name", "units"},
						prometheus.Labels{
							"hostname": "foo",
							"machine":  "bar",
						}),
					prometheus.GaugeValue,
					2, []string{"3", "2", "n4", "unknown"}...,
				),
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c := &PrometheusCollector{
				Context:     tt.fields.Context,
				MachineName: tt.fields.MachineName,
				Hostname:    tt.fields.Hostname,
			}
			c.Collect(tt.args.metrics)
			close(tt.args.metrics)
			mm := []prometheus.Metric{}
			for m := range tt.args.metrics {
				mm = append(mm, m)
			}
			assert.Equal(t, tt.wantMetrics, mm)
		})
	}
}
