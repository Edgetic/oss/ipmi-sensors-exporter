// TODO: this package will become a prometheus exporter

package ipmi

import (
	"errors"
	"fmt"
	"strings"

	lib "gitlab.com/edgetic/oss/ipmi-sensors-exporter/pkg/ipmi/libipmimonitoring"
)

// SensorsConfig exposed to configure ipmi sensor readings
type SensorsConfig struct {
	// ProtocolVersion string
	ProtocolVersion string
	// Host to connect to
	Host string
	// Username for auth
	Username string
	// Password for auth
	Password string
	// CacheDir caching directory for SDR info files
	// needs to be writable by this process
	CacheDir string
	// PrivilegeLevel used for context session
	PrivilegeLevel string
	// AuthenticationType for context auth
	AuthenticationType string
	// CipherSuite id of Cipher suite
	CipherSuite *int
	// WorkaroundFlags used for weird BMC implementations
	WorkaroundFlags []string
}

// Manager provides access to multiple contexts
type Manager struct {
	manager lib.IPMIManager
}

// NewManager returns a new IPMI manager
func NewManager() Manager {
	return Manager{
		manager: lib.NewManager(),
	}
}

// Init must be called before accessing context
func (m Manager) Init() error {
	return m.manager.Init()
}

// Context takes a sensors config and returns a context
// errors returned validating config or creating context
// context must be closed after use
func (m Manager) Context(c SensorsConfig) (lib.IPMISensorContext, error) {
	cc, err := makeContextConfig(c)
	if err != nil {
		return nil, fmt.Errorf("Error making ipmi config: %w", err)
	}

	sc, err := m.manager.SensorContext(*cc)
	if err != nil {
		return nil, fmt.Errorf("Error creating ipmi sensor context: %w", err)
	}

	return sc, nil
}

func makeContextConfig(c SensorsConfig) (*lib.ContextConfig, error) {
	dc := lib.DefaultConfig()

	pv := dc.ProtocolVersion
	f, err := parseNameFlag(pv, c.ProtocolVersion, int(dc.ProtocolVersion))
	if err != nil {
		return nil, fmt.Errorf("Error parsing protocol version: %w", err)
	}
	pv = lib.ProtocolVersion(f)

	host := c.Host
	if host == "" {
		return nil, errors.New("Host must not be empty")
	}

	pl := dc.PrivilegeLevel
	f, err = parseNameFlag(pl, c.PrivilegeLevel, int(pl))
	if err != nil {
		return nil, fmt.Errorf("Error parsing privilege level: %w", err)
	}
	pl = lib.PrivilegeLevel(f)

	at := dc.AuthenticationType
	f, err = parseNameFlag(at, c.AuthenticationType, int(at))
	if err != nil {
		return nil, fmt.Errorf("Error parsing authentication type: %w", err)
	}
	at = lib.AuthenticationType(f)

	cs := dc.CipherSuite
	if c.CipherSuite != nil {
		f, err = validateCipherSuite(*c.CipherSuite)
		if err != nil {
			return nil, fmt.Errorf("Error validating cipher suite: %w", err)
		}
		cs = lib.CipherSuite(f)
	}

	wf := dc.WorkaroundFlags
	wfn := lib.WorkaroundFlagNamer{
		ProtocolVersion: pv,
	}
	f, err = parseBitmaskFlags(wfn, c.WorkaroundFlags, int(wf))
	if err != nil {
		return nil, fmt.Errorf("Error parsing workaround flag bitmasks: %w", err)
	}
	wf = lib.WorkaroundFlags(f)

	return &lib.ContextConfig{
		ProtocolVersion:    pv,
		Host:               host,
		Username:           c.Username,
		Password:           c.Password,
		CacheDir:           c.CacheDir,
		PrivilegeLevel:     pl,
		AuthenticationType: at,
		CipherSuite:        cs,
		WorkaroundFlags:    wf,
	}, nil
}

func parseNameFlag(fn lib.FlagNamer, n string, def int) (int, error) {
	if n == "" {
		return def, nil
	}

	nz := normalizeFlagName(n)
	f := fn.NameFlag(nz)
	if f == -1 {
		return def, fmt.Errorf("Flag not found for %s", n)
	}
	return f, nil
}

func parseBitmaskFlags(fn lib.FlagNamer, nn []string, def int) (int, error) {
	if len(nn) == 0 {
		return def, nil
	}

	ff := make([]int, len(nn))
	for i, n := range nn {
		f, err := parseNameFlag(fn, n, def)
		if err != nil {
			return def, err
		}
		ff[i] = f
	}
	acc := 0
	for _, f := range ff {
		acc |= f
	}
	return acc, nil
}

func normalizeFlagName(n string) string {
	// support UPPER_SNAKE_CASE versions of flag names
	n = strings.ToLower(n)
	return strings.ReplaceAll(n, "_", " ")
}

func validateCipherSuite(in int) (int, error) {
	switch lib.CipherSuite(in) {
	case lib.CipherSuiteHMACMD5,
		lib.CipherSuiteHMACMD5_128,
		lib.CipherSuiteHMACMD5_128_AESCBC128,
		lib.CipherSuiteHMACMD5_MD5128,
		lib.CipherSuiteHMACMD5_MD5128_AESCBC128,
		lib.CipherSuiteHMACSHA1,
		lib.CipherSuiteHMACSHA1_96,
		lib.CipherSuiteHMACSHA1_96_AESCBC128,
		lib.CipherSuiteHMACSHA256,
		lib.CipherSuiteHMACSHA256_128,
		lib.CipherSuiteHMACSHA256_128_AESCBC128,
		lib.CipherSuiteNone:
		return in, nil
	}
	return 0, fmt.Errorf("Could not find cipher suite for id %d", in)
}
