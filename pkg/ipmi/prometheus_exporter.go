package ipmi

import (
	"fmt"
	"strconv"
	"strings"

	"github.com/prometheus/client_golang/prometheus"
	lib "gitlab.com/edgetic/oss/ipmi-sensors-exporter/pkg/ipmi/libipmimonitoring"
	"k8s.io/klog/v2"
)

// PrometheusCollector implements prometheus collector interface
type PrometheusCollector struct {
	// Context for ipmi access
	Context lib.IPMISensorContext
	// MachineName identifier
	MachineName string
	// Hostname of remote BMC
	Hostname string
}

// Describe is not required
func (c *PrometheusCollector) Describe(desc chan<- *prometheus.Desc) {}

// Collect implements prometheus collector body
func (c *PrometheusCollector) Collect(metrics chan<- prometheus.Metric) {
	const (
		namespace = "ipmi_sensors"
		error     = "error"
		hostname  = "hostname"
		machine   = "machine"
		recordId  = "record_id"
		number    = "number"
		name      = "name"
		units     = "units"
	)

	sens, err := c.Context.ReadSensors()
	if err != nil {
		klog.Errorf("Error reading ipmi sensors: %v", err)
		metrics <- prometheus.NewInvalidMetric(prometheus.NewDesc(prometheus.BuildFQName(namespace, "", error), "", nil, nil), err)
		return
	}

	constLabels := prometheus.Labels{
		hostname: c.Hostname,
		machine:  c.MachineName,
	}

	labels := []string{recordId, number, name, units}

	for _, s := range sens {
		// unknown sensor reading types are usually state info or events
		// either way, we can't expose them as metrics
		if s.ReadingType == lib.SensorReadingTypeUnknown {
			continue
		}
		reading := c.mustReadingToGauge(s.ReadingType, s.Reading)
		metrics <- prometheus.MustNewConstMetric(
			prometheus.NewDesc(prometheus.BuildFQName(namespace, "", strings.ReplaceAll(s.Type.String(), " ", "_")),
				fmt.Sprintf("IPMI sensor reading for type %s", s.Type), labels, constLabels),
			prometheus.GaugeValue,
			reading,
			strconv.Itoa(s.RecordId), strconv.Itoa(s.Number), s.Name, s.Units.String())
	}
}

func (c *PrometheusCollector) Close() {
	if c.Context != nil {
		c.Context.Close()
	}
}

func (c *PrometheusCollector) mustReadingToGauge(sensType lib.SensorReadingType, reading interface{}) float64 {
	switch sensType {
	case lib.SensorReadingTypeBool:
		v := reading.(bool)
		if v {
			return 1
		}
		return 0
	case lib.SensorReadingTypeInt32:
		return float64(reading.(int32))
	case lib.SensorReadingTypeFloat64:
		return reading.(float64)
	}
	panic(fmt.Errorf("Reading type %s unknown", sensType))
}
