.PHONY: build
build:
	mkdir -p out/build
	go build -o ./out/build ./cmd/...

.PHONY: lint
lint:
	go run github.com/golangci/golangci-lint/cmd/golangci-lint run

.PHONY: test
test:
	go test -tags=none -race -cover ./...

.PHONY: run-exporter
# requires access to real remote BMC
# use environment variables and ./configs/ipmi-exporter.yaml to configure
run-exporter:
	go run ./cmd/exporter

.PHONY: exporter-help
exporter-help:
	go run ./cmd/exporter --help

.PHONY: helm-lint
helm-lint:
	helm lint deploy/charts/ipmi-sensors

export GO_VERSION = 1.15.6
SKAFFOLD_FILE = ./deploy/skaffold.yaml

.PHONY: skaffold-build
skaffold-build:
	skaffold build -f $(SKAFFOLD_FILE)

.PHONY: skaffold-run
skaffold-run:
	skaffold run --detect-minikube -f $(SKAFFOLD_FILE)

.PHONY: skaffold-delete
skaffold-delete:
	skaffold delete --detect-minikube -f $(SKAFFOLD_FILE)