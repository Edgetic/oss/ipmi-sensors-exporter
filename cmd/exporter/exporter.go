package main

import (
	"context"
	"fmt"
	"net/http"

	"github.com/davecgh/go-spew/spew"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/spf13/cobra"
	"github.com/spf13/pflag"
	"github.com/spf13/viper"
	"gitlab.com/edgetic/oss/ipmi-sensors-exporter/internal/cmd"
	"gitlab.com/edgetic/oss/ipmi-sensors-exporter/pkg/ipmi"
	"gitlab.com/edgetic/oss/ipmi-sensors-exporter/pkg/ipmi/libipmimonitoring"
	"k8s.io/klog/v2"
)

// Config for exporter
type Config struct {
	// Host address of the IPMI server
	Host string
	// CacheDir caching directory for SDR
	CacheDir string `mapstructure:"cache-dir"`
	// User auth user
	User string
	// Password file auth value
	Password string
	// Protocol version for IPMI LAN
	ProtocolVersion string `mapstructure:"protocol-version"`
	// Privilege level for IPMI auth
	PrivilegeLevel string `mapstructure:"privilege-level"`
	// AuthenticationType for context auth
	AuthenticationType string `mapstructure:"authentication-type"`
	// CipherSuite id of Cipher suite
	CipherSuite *int `mapstructure:"cipher-suite"`
	// WorkaroundFlags used for weird BMC implementations
	WorkaroundFlags []string `mapstructure:"workaround-flags"`
	// Port for prometheus exporter server
	Port uint
	// MachineName used in exporter as friendly name
	MachineName string `mapstructure:"machine-name"`
}

func Execute() {
	vpr := viper.GetViper()
	config := &Config{}

	rootCmd := &cmd.RootCmd{
		Use:            "exporter",
		Short:          "IPMI Sensor Exporter",
		Long:           "IPMI Exporter converts IPMI sensor readings into Prometheus measurements. Deploying into K8s also provides Influxdb exporting via Telegraf.",
		EnvPrefix:      "IPMI",
		ConfigFlag:     "config",
		ConfigFileName: "ipmi-exporter",
		InitFlags:      initFlags,
		Run: func(ctx context.Context, args []string) error {
			if err := vpr.Unmarshal(config); err != nil {
				return err
			}

			if err := run(ctx, config); err != nil {
				return err
			}

			return nil
		},
	}

	rootCmd.Execute()
}

func initFlags(_ *cobra.Command, fs *pflag.FlagSet) {
	fs.StringP("host", "a", "", "host address for the IPMI server")
	fs.String("machine-name", "", "machine name used for exporter labels")
	fs.String("cache-dir", "./cache", "cache directory for sensor data")
	fs.StringP("user", "u", "", "User for IPMI auth")
	fs.StringP("password", "p", "", "Password for IPMI auth")
	fs.String("protocol-version", "2.0", "IPMI LAN protocol version (1.5 or 2.0)")
	fs.String("privilege-level", "USER", "IPMI privilege level (USER, OPERATOR or ADMIN)")
	fs.String("authentication-type", "MD5", "IPMI auth type (NONE, STRAIGHT_PASSWORD_KEY, MD2 or MD5)")
	fs.Int("cipher-suite", 3, "IPMI cipher-suite (many options, see https://linux.die.net/man/8/ipmi-sensors)")
	fs.StringSlice("workaround-flags", []string{"NONE"}, "IPMI workaround flags (many options, see https://linux.die.net/man/8/ipmi-sensors)")
	fs.Uint("port", 2112, "Port number of prometheus exporter endpoint")
}

func run(ctx context.Context, config *Config) error {
	cf := ipmi.SensorsConfig{
		Host:               config.Host,
		Username:           config.User,
		Password:           config.Password,
		CacheDir:           config.CacheDir,
		ProtocolVersion:    config.ProtocolVersion,
		PrivilegeLevel:     config.PrivilegeLevel,
		AuthenticationType: config.AuthenticationType,
		CipherSuite:        config.CipherSuite,
		WorkaroundFlags:    config.WorkaroundFlags,
	}

	config.Password = "***"
	klog.V(2).Infof("Using config: %s", spew.Sdump(config))

	manager := ipmi.NewManager()
	if err := manager.Init(); err != nil {
		return err
	}

	ipmiCtx, err := manager.Context(cf)
	if err != nil {
		return err
	}
	defer ipmiCtx.Close()

	go metrics(*config)

	collector(*config, ipmiCtx)

	<-ctx.Done()
	return nil
}

func metrics(c Config) {
	klog.V(1).Infof("Starting Prometheus metrics server on %d", c.Port)
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "text/html")
		fmt.Fprint(w, `IPMI Metrics are at <a href="/metrics">/metrics<a>`)
	})
	http.Handle("/metrics", promhttp.Handler())
	if err := http.ListenAndServe(fmt.Sprintf(":%d", c.Port), nil); err != nil {
		klog.Errorf("HTTP server error: %v", err)
	}
}

func collector(c Config, ctx libipmimonitoring.IPMISensorContext) {
	n := c.MachineName
	if n == "" {
		n = c.Host
	}

	collector := &ipmi.PrometheusCollector{
		Context:     ctx,
		MachineName: n,
		Hostname:    c.Host,
	}
	prometheus.MustRegister(collector)
}
