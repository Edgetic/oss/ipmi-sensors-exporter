# IPMI Sensors

Collecting remote IPMI sensor data and exporting to Prometheus/Influxdb.

**Out-of-band (i.e. LAN) IPMI only**, we're not supporting in-band drivers.

## Build Prerequisites

- Golang
- libipmimonitoring - (Install `libipmimonitoring-dev` on Ubuntu)

## Dev Prerequisites

- Helm v3+
- Skaffold v1.17+
- Minikube (for testing)

## Running

Run the exporter locally by doing something like this:
```
IPMI_USER=Administrator IPMI_PASSWORD=BLAH go run ./cmd/exporter/ -v2 --machine-name $HOSTNAME
```

## Kubernetes Deployment

Packaged as a Helm chart, see `./deploy/charts/ipmi-sensors`.

You can also locally build and deploy via Skaffold, see `./deploy/skaffold.yaml`.

## Config

All config values can be set via config file, CLI flags or environment variables.

Config fields:

- `host` - hostname of remote IPMI BMC
- `cache-dir` - caching directory used for IPMI SDR info
- `user` - username for IPMI auth
- `password` - password for IPMI auth
- `protocol-version` - version of remote IPMI protocol
- `privilege-level` - privilege level required for IPMI session
- `authentication-type` - auth type for IPMI session
- `cipher-suite` - cipher suite used for IPMI connection
- `workaround-flags` - workaround flags for IPMI issues
- `port` - port exposed for Prometheus metrics server
- `machine-name` - machine name used in prometheus metrics

See `./pkg/ipmi/libipmimonitoring/names.go` for possible values.

## libipmimonitoring

We use [libipmimonitoring](https://linux.die.net/man/3/libipmimonitoring) to integrate with the remote BMC via **cgo** and Golang.

The main C header file that we integrate with is [ipmi_monitoring.h](http://git.savannah.gnu.org/cgit/freeipmi.git/tree/libipmimonitoring/ipmi_monitoring.h.in).

libipmimonitoring git source is here, with other useful header files - http://git.savannah.gnu.org/cgit/freeipmi.git/tree/libipmimonitoring.

## Future Work

- Currently a real BMC must be accessible for development.
    - We could use OpenIPMI lan simulator to create a fake BMC for local testing.
    - See man pages for `ipmi_sim`, `ipmi_lan` and `ipmi_sim_cmd` for more details.
- Support multiple hosts